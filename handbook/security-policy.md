# Tangible AI Security Policy

## Purpose of this document 

Tangible AI must protect restricted, confidential, or sensitive data from loss to avoid reputation damage and to avoid adversely impacting our customers. The protection of data in scope is a critical business requirement, yet flexibility to access data and work effectively is also critical.

It is not anticipated that this technology control can effectively deal with the malicious theft scenario, or that it will reliably detect  all data. Its primary objective is user awareness and to avoid accidental loss scenarios. This policy outlines the requirements for data leakage and exposure prevention, a focus for the policy, and a rationale.  

## Who to contact about security issues and incidents 

Please contact security@tangibleai.com with any security-related incidents or issues.

## Security Process and Procedures for Team Members

### Accounts and Passwords

1. Read and follow the requirements for handling passwords and other credentials in
  the [Tangible AI Password Policy Guidelines](#tangible-ai-password-policy-guidelines) below
  for all accounts used to conduct Tangible AI related work.
  Using BitWarden to generate and store the passwords is strongly recommended.
1. Set up [BitWarden] as your password manager and set a **strong and unique**
  master password.
   - Keep your Master Password a secret. No other team members should know it, including admins. If the Master Password is known or disclosed to someone else, it should be changed immediately.
   - Notify security@tangibleai.com if you forgot your Master Password.
   - Consider using a generated Master Password. Most human-created passwords
   are easy to guess. Let BitWarden create a strong Master Password. But: you _will_
   need to memorize this Master Password.
   - Do not let your password manager store the **master password**. It is okay to
     store the username.
   - For more information, review [Bitwarden Getting Started guide](https://bitwarden.com/help/create-bitwarden-account/)
   that guides you through the sign-up process.
1. Enable two-factor authentication (2FA) with an authenticator, such as
  [Google authenticator] for on every account that supports
  it. This is required for
  [Google](https://myaccount.google.com/signinoptions/two-step-verification/enroll-welcome),
  [GitLab](https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html)
  [Digital Ocean](https://docs.digitalocean.com/products/accounts/security/2fa/)
  If any systems provide an option to use SMS text as a second factor, this is highly discouraged.
  Phone company security can be easily subverted by attackers allowing them to take over a phone account.
  _(Ref: [6 Ways Attackers Are Still Bypassing SMS 2-Factor Authentication](https://www.securityweek.com/6-ways-attackers-are-still-bypassing-sms-2-factor-authentication) / [2 minute Youtube social engineering attack with a phone call and crying baby](https://www.youtube.com/watch?v=lc7scxvKQOo))_

### Data Security

### Secure Coding Guidelines 
All Tangible AI team members that participate in software development should abide by Security Coding Practices as defined by [OWASP](https://owasp.org/www-project-secure-coding-practices-quick-reference-guide/migrated_content).

## Tangible AI Password Policy Guidelines

Passwords are one of the primary mechanisms that protect Tangible AI information systems and other resources from unauthorized use. Constructing secure passwords and ensuring proper password management is essential. Tangible AI's password guidelines are based, in part, on the recommendations by [NIST 800-63B](https://pages.nist.gov/800-63-3/sp800-63b.html). To learn what makes a password truly secure, read this [article](https://medium.com/peerio/how-to-build-a-billion-dollar-password-3d92568d9277) or watch this [conference presentation](https://www.youtube.com/watch?v=vudZnjp5Uq0&t=19183) on password strength.

### Tangible AI team-members Password Requirements
* At Tangible AI, we enforce a strong password requirement, which consists of minimum length of 12 characters.
* To make a secure password you can remember, consider using a [combination of 3 or more random words](https://medium.com/peerio/how-to-build-a-billion-dollar-password-3d92568d9277#67c2)
* The use of special characters is not required or even recommended.
* Avoid creating predictable passwords.
* Passwords cannot be reused.
* Passwords should not be the same as username.
* Passwords should be unique from the previous passwords used.

### Password Management
* Password "hints" are not to be used. If a password is forgotten, a mechanism must be in place to replace a password/passphrase with sufficient controls to verify the identity of the requester of the password reset.
* Passwords must be stored in a way that is resistant to offline attacks and must be salted and hashed using a suitable one-way key derivation function.
* If a password is required to be stored, it must be stored within an approved password manager application and may be pasted from this using a master password function (e.g. BitWarden).
* Passwords are to be kept private and secured.
* If an account or password is suspected to have been compromised, report the incident to security@tangibleai.com.
* Passwords are not to be shared or be in clear text or be written down.
* Security questions like "What is your favorite color? What is your mother's maiden name?", etc should be answered with a random non-obvious word or set of words. 

### System Password Requirements
* For systems where a password can be configured the minimum password length needs to be set to 12 characters.
* To make a secure password you can remember, consider using a [combination of 5 or more random words](https://medium.com/peerio/how-to-build-a-billion-dollar-password-3d92568d9277#67c2)
* The use of special characters is not required.
* If a particular system will not support 12 character passwords, then the maximum number of characters allowed by that system shall be used.
* If a particular system requires a password history, configuration should be set for 25 remembered passwords.
* Passwords are not acceptable if they match the subsequent patterns, and must be checked against commonly used or expected patterns, including known breached password lists, dictionary words, repetitive or sequential characters, or context specific words, such as the name of the service, username, or derivatives thereof.
* System administrators of applications and or devices must change default passwords.
* System administrators need to enable password strength on third party applications and or tools, where applicable.
* For applications where a password is the only source of authentication a password must be expired within a maximum of 90 calendar days.
* Systems should monitor and log failed login attempts.
* Authentication failed login attempts information needs to be recorded within the application logs such as: name, date, number of failed attempts, unique log identifier.
* Repeated failed login attempts must trigger a temporary account lockout after 10 failed attempts. The lockout may end after a designated period of time, or require a manual unlock, depending on the profile of the application.

### Two Factor Authentication

All Tangible AI team members should use [Two Factor Authentication](https://en.wikipedia.org/wiki/Multi-factor_authentication) (2FA) whenever possible. Usage of 2FA by Tangible AI team members is **required** for access to the production environment. It should be noted that references to MFA (Multi-Factor Authentication) are often included in language associated with third party products and certain Compliance references, but the general concept is still covered by the term "2FA". There are different 2FA methods that can be used by Tangible AI team members. These are ranked by security strength:

- [Push Authentication](https://en.wikipedia.org/wiki/Authenticator#Mobile_Push). For Push Authentication to work, the authentication service and a complementary mobile app typically use RSA keys and OOB (Out Of Band) communications to perform the secondary authentication.
- [TOTP](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm). TOTP (Time-based One Time Password) is a popular method for a second factor. This can be done via a mobile app (Google Authenticator, Duo Security, etc) although there are some software implementations as well. While not as secure as U2F or Push as TOTP could be [phished](https://en.wikipedia.org/wiki/Phishing) (although the attack window would be extremely short), it is still a very secure method of authentication.
- [SMS](https://en.wikipedia.org/wiki/SMS). SMS (Short Message Service) is a method of using text messaging to provide out-of-band (OOB) authentication. As the messages can be spoofed or intercepted more easily than other methods, SMS is the least recommended method for 2FA. 

### BitWarden Guidelines

1. Do not share credentials via email, issue comments, chat etc. This includes
   email addresses to login and API keys. Use BitWarden vaults for this.
1. If you do not have access to a vault that is part of the baseline entitlements
  for your role and team, report it to Tangible AI manager you're working with (Hobson or Maria).
1. Do not copy passwords from inside a BitWarden vault to a personal password
vault or other password store. BitWarden should be the only password
vault used for teams. Team passwords should not be duplicated or placed in
personal password vaults where they can potentially be exposed to compromise.
1. When asked security questions (what is your favorite pet, etc.) do not answer
truthfully since that is easy to research. Make up an answer and write both the
question and answer in Bitwarden. 

## Security Awareness Training

The New Hire training is a personal training conducted by a Tangible AI team member, and needs to be completed by every new employee or contractor. 

The objectives of the security training are: 
1. Make all Tangible AI team-members aware of the importance of their role in securing Tangible AI on a daily basis, and to empower them to make the right decisions with security best-practices.
1. Familiarize all new Tangible AI team-members with security-related situations that they might encounter during their time with the company.

### Security Training Delivery

The training will be delivered in-person or virtually (by Zoom) and cover the following:

  * Tangible AI Password Policy.
  * [Acceptable Use](/handbook/acceptable-use-policy.md)
  * [Data Classification](/handbook/engineering/security/data-classification-standard.html)

### Phishing

#### How to identify a basic phishing attack

When you receive an email with a link, hover your mouse over the link or view
the source of the email to determine the link's true destination.

If you hover your mouse cursor over a link in Google Chrome it will show you
the link destination in the status bar at the bottom left corner of your browser
window.

![Hover Example](/images/phishing/hover-status-bar-example-chrome.png)

In Safari the status bar must be enabled to view the true link destination
(View -> Show Status Bar).

Some examples or methods used to trick users into entering sensitive data into
phishing forms include:

* Using HTTP(S) with a hostname that begins with the name of a trusted
site but ends with a malicious site.

![Malicious Domain](/images/phishing/malicious-domain.png)

* Using a username or password inside the request that corresponds to the name
of a trusted domain and assuming the viewer won't view the whole URL.

![Trick Username](/images/phishing/username-password.png)

* Using a data URI scheme instead of HTTP(S) is a particularly devious means of
tricking users. Data schemes allow the embedding of an entire web page inside
the URI itself. Data schemes will not show the typical green lock in the address
bar of a browser that is customarily associated with a verified SSL connection.

![Data Scheme](/images/phishing/data-scheme.png)

When viewing the source of an HTML email it is important to remember that the
text inside the "HREF" field is the actual link destination/target and the text
before the `</A>` tag is the text that will be displayed to the user.

`<a href="http://evilsite.example.org">Google Login!</a>`

In this case, "Google Login!" will be displayed to the user but the
actual target of the link is "evilsite.example.org".

After clicking on a link always look for the green lock icon and "secure" label
that signify a validated SSL service. This icon alone is not enough to verify the
authenticity of a website, however the lack of the green icon does mean you
should never enter sensitive data into that website.

![Green Lock Example](/images/phishing/green-lock-example.png)

### What to do if you suspect an email is a phishing attack 

If you suspect that the email is targeted specifically at you or GitLab, 
please 

1. Forward the email as attachment directly to security@tangibleai.com

2. Report the email as `phishing` in Gmail: 

Gmail offers the option to report the email directly to Google as a
phishing attempt, which will result in its deletion. Reporting the email
in this manner will help the security team track phishing metrics and trends over
time within Google Workspace.

To report the email as `phishing` via built-in Gmail reporting:

  1. Select the "More" button (three dots) on the email in question
  1. Choose "Report phishing" option from the drop down menu

If you receive an email that appears to come from a service that you utilize,
but other details of the email are suspicious -- a private message from a
sender you don't recognize, for example -- do not click on any links in the
email. Instead use your own bookmark for the site or manually type the address
of the website into your browser.

Unsolicited email should be treated as phishing emails. For example, if you
did not register for a site claiming to send you email, do not click on links
in the email or visit the site.

## Data Security 

Our data is protected "in depth" by ensuring that all private user data is encrypted in transit and at rest.
All user data is stored on either managed database servers or within an "Volume" or external drive that encrypts all data at rest:

- Digital Ocean Droplet Volume
- AWS EBS Volume
- all GCP storage

Digital Ocean, GCP, and AWS managed PostgreSQL instances all have encrypted-at-rest persistent storage by default.

### Credentials

Credentials are the user names, tokens, keys, and passwords) required to access a service, account, or database.
Software for our websites, chatbots, devops pipelines, python packages, and automation scripts often require access to credentials  
We store credentials in .env files in the path `~/.ssh/.env/`.
Files are named according to the project (e.g. `~/.ssh/.env/poly.env`) and sourced from within the virtual environment (or conda environment) activation scripts.

Whenever an employee or contractor finishes a project they remove the appropriate keys from their .ssh/.env/ directory.
Credentials (including ssh keys) are retained in BitWarden for 3 months after the conclusion of the project.
If customers void or cancel or close a contract, all credentials associated with that project are deleted on all employee devices and cloud password management services (BitWarden).
Periodic audits are performed to delete all unused credentials.

## Auditing

Security audits or certification are often required by our customers to reassure them and their users that their data is secure.

### Certifications

Various levels or kinds of security certifications may be requested by our customers and users:

- SOC-2
- GDPR
- HIPPA

We perform internal audits and external audits as required to meet the requirements of these certifications.

### Internal Audits

Our CI-CD pipeline and devops automation are an integral part of our data protection processes. 
We employ automated testing tools and linting tools to ensure that our code is correct and does not expose sensitive data, including security keys and passwords.
We utilize [GitGuardian]()'s open source software packages that work with git to ensure you do not accidentally expose plaintext credentials within git repositories.
We add customized pre-commit hooks that run linters and credential scanners to expand the kinds of credentials we block before getting pushed to shared git repositories. GitGuadian would otherwise miss many of our tokens and passwords because it is mainly used for AWS credentials and would miss many SSH keys and Digital Ocean credentials. 

The CTO and the project manager are responsible for ensuring that all engineers understand this security policy document.
Managers perform "spaced-repetition" quizzing of engineers.
This focuses training and automation resources on the security concepts that are most useful to staff.

### External Audits

[Vault.com](https://www.vanta.com/products/) offers automated services for SOC-2, GDPR, HIPPA audits as well as network monitoring.
[GitGuardian]() offers a paid service for source code scanning and vulnerability assessment.
