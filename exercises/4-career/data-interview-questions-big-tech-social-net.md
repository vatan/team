# 2021 BigTech Social Network R&D Data Analyst Interview Notes
-
Q: Influencing: How do you confidently communicate to both technical and not technical audiences in order to drive strategy?
-
Q: SQL Coding: How do you manage data and manipulate data?
-
Q: How do you calculate variance of lift?
-
Q: How would you explain P-Value?
-
Q: Define SQL functions: mergers, hashes joins, differences between inner vs outer joins etc.

## Example Problem posed followed up by Questions:

-
Q: How many TVs in the US?
-
Q: Sampling bias question: Suppose you have a sample that is not representative of the population that you're interested in measuring. (Ex: Measure the impact of a nationwide advertising campaign, but only have data for part of the country. e.g. UK campaign, data for London) Do you see a problem with this? If so, what?
-
Q: Things to consider: London only sample is not representative
-
Q: ecological impact inference
-
Q: UK sample distribution
-
Q: country level 50/50 London has 70/30 percent male female weigh sample make
