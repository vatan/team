# Data Ethics

Before you start mining your user data for insights or using it to train models you must consider the privacy of your users.

## Anonymization

"But we anonymized our data so we're good to go, right?"

No you haven't, and no you aren't. 
Perfect anonymization, like perfectly secure servers, is not possible. 
Think about it. 
Did you ask your users if they could release your supposedly anonymous version their data to the public or use it to extract wealth from them or others? 
Did you ask them to review your anonymization algorithms?
Are you sure they would approve?
Would you trust Facebook to anonymize your data and release your private thoughts to the public or exploitative marketers or even hackers?

So annonymization doesn't count.
It's a nice bonus to add to your process to give an extra layer of protection.
But it's not a complete data security policy or process.

## Resources

- [Lessons on technical data protection](https://towardsdatascience.com/ethical-data-work-lessons-on-technical-data-protection-e6ab2d0c6571)
