# Assignment: A Robust Fake News Detector
​
Your first NLP problem was to detect fake news.
Because the dataset contained a lot of leakage it was relatively easy to solve.
All the fake news articles came from one of two sources and the genuine articles came from a different set of news sources.
So the titles and article texts had several keywords and capitilizations peculuar to those news sources.
It's possible to create a model with 99% accuracy on the test set (better than a human) with just a few numerical features.

- Count of the all-caps words in the title
- Use of words like "Trump", "Hillary", or "Obama"
- Inclusion of words like "Reuters" or authors' names
- The news source categorical variable (this alone gives 100% accuracy)

This is what is called a "brittle" ML pipeline.
It's not your fault.
The dataset is very leaky, and dirty.
It contains horrible sampling bias because it doesn't provide a representative sample of news articles from the real world.
At least some genuine, truthful statements and some misinformation statements or articles should be found from each news source.

A hot topic for research these days is building more robust deep learninge pipelines, especially for NLP.
This first requires improving the quality of your problem statement and dataset.
Here's [an article](http://web.stanford.edu/~mattm401/docs/2018-Golbeck-WebSci-FakeNewsVsSatire.pdf) about a better dataset.
And here's [the dataset](https://github.com/jgolbeck/fakenews/blob/master/FakeNewsData.zip)

