# Competitions and Challenges

## Machine Learning Competitions

Sign up for some ML competitions and find some fun projects to work on:

1. Numer.ai - stock market forecasting/trading
2. Bitgrit https://bitgrit.net/competition/ - like kaggle
3. Kaggle
4. Codalab https://competitions.codalab.org/competitions/ - grammar, OCR, parametric 3d skelletons, metalearning (learning rate reinfircement learning)
5. Driven data https://www.drivendata.org/competitions/ - forestry  management, privacy anonymizers
6. DataHack https://datahack.analyticsvidhya.com/
7. MachineHack https://machinehack.com/ - recruiter job market (predict book prices, flight tickets)
8. Zindi https://zindi.africa/competitions Africa kaggle
9. AI crowd https://www.aicrowd.com/ - neurips openai sponsored, language assisted human ai collaboration in minecraft
10. Iron Viz tableau bs
11. IASCASC Data analysis competition 2021 https://iasc-isi.org/2020/09/10/data-analysis-competition-2021-edition/ academic poster project https://iasc-isi.org/2022/10/11/call-of-the-data-analysis-competition-2023/ international association statistical computing
12. * Codalab https://competitions.codalab.org/competitions/ -  SOTA problems
13. Bitgrit https://bitgrit.net/competition/ -
14. [Crowd Analytix](https://www.crowdanalytix.com/) - advertising anaytics
15. https://en.m.wikipedia.org/wiki/Competitions_and_prizes_in_artificial_intelligence)


### Numer.ai
Stock market competition hedge fund that anonymizes Bloomberg data

### Ubuquiant in China
https://www.kaggle.com/competitions/ubiquant-market-prediction/leaderboard

### Kaggle.com
Boosted Trees and neural nets win most competitions

### Sports
https://www.pronocontest.com/

### Metaculus.com

## Research AGI 

Find examples of AGI and read the news and paperswithcode.com about AGI.
Then contribute to Wikipedia articles on the topic: 

https://en.m.wikipedia.org/wiki/Competitions_and_prizes_in_artificial_intelligence

## Coding Competitions

Level up your Python coding skills by competing and practicing on coding challenge websites

* Topcoder open 2021 programming comp https://www.topcoder.com/community/member-programs/topcoder-open/
* Exercism.io
* Codility

## Some hard competitions in AGI

### Conversational NLP

Amazon Alexa Social bot

## Economics & Finance 

### web traffic time series
https://www.kaggle.com/c/web-traffic-time-series-forecasting
Web traffic 145k Wikipedia pages

### M5 time series forecasting
Walmart purchase prediction 
https://www.kaggle.com/c/m5-forecasting-accuracy
"The future of forecasting competitions: Design attributes and principles" by Spyros Makridakis, Chris Fry, Fotios Petropoulos, and Evangelos Spiliotis.
other time-series researchers, such as Rob Hyndman

### microprediction
https://www.microprediction.com/blog/future
Recommendations for competition datasets :
1. Scope: Predictions of uncertainty are critical to the usefulness and evaluation of algorithns
2. Diversity : Data diversity to ensure generalizations are valid 
3. No cheating (external data only allowed it available at time of competition)
4. Microprediction  and nowcasting
5. Encourage outside data discovery
6. Aggregating forecasts across multiple time horizons 
7. Rolling competitions that reveal data over time

### choice prediction 
Predict human behavior including rational value (conventional econ) and behavioral econ (fallacies and biases)
 https://cpc-18.com/
Three approaches
 1. pure behavioral models like BEAST (Best Estimate And Simulation Tools)
 2. pure machine learning tools like Hartford et al.’s deep learning architecture
 3. a mixture of behavioral and data-driven approaches, like Psychological Forest
 
 BEAST and variations of it ( statistical models ) worked best.
 http://oriplonsky.com/wp-content/uploads/2017/09/Erev-et-al-2017.pdf
 14 of the classical phenomena in dataset, including the St. Petersburg paradox, (Bernoulli, 1954), the Allais paradox, (Allais, 1953), and the Ellsberg paradox, (Ellsberg, 1961)
 Best Estimate And Simulation Tools), assumes choice is sensitive to the expected values and to four additional behavioral tendencies: (a) minimization of the probability of immediate regret, (b) a bias toward equal weighting of possible outcomes, (c) sensitivity to the payoff sign, and (d) pessimism.
 Psychological (random) Forest model outperforms pure statistical ML models

### algebra word problems
Academic benchmark with 100k+ examples? Synthesized variations?


## Computer Science Resources

- [Python CS101](https://runestone.academy/ns/books/published/fopp/index.html)
- [Javascript CS101](https://eloquentjavascript.net/)
