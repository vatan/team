# Data Science for Digital Health

These are the exercises from the UCSD Extension course "Data Science for Digital Health".

3. [Multivariate Regression with Python
    - [Instructions](README-3-multivariate-regression.md)
    - [Jupyter notebook](Lesson_3_Multivariate_Regression_with_Python.ipynb)
    - [Cloud notebook](https://colab.research.google.com/drive/1MbwdnqNqghU9eliCg6l4YaaDBU2c7T-n?usp=sharing)


## [Excercise 3: Multivariate Regression with Python](https://colab.research.google.com/drive/1MbwdnqNqghU9eliCg6l4YaaDBU2c7T-n?usp=sharing)

### 1. Copy the Jupyter Notebook titled "Lesson 3: Multivariate Regression with Python.ipynb": 

[https://colab.research.google.com/drive/1MbwdnqNqghU9eliCg6l4YaaDBU2c7T-n?usp=sharing](https://colab.research.google.com/drive/1MbwdnqNqghU9eliCg6l4YaaDBU2c7T-n?usp=sharing)

### 2. Run the code one cell at a time using "active learning" to predict in your mind what the code will do.

Read the instructions in Markdown text (normal English instructions) above each cell containing Python code.
Select the first cell with your mouse or touchpad by clicking on the cell.
Hold down the shift key as you hit the Enter on your keyboard ([SHIFT]-[ENTER]) after selecting a cell (either Markdown or Python).
The [SHIFT]-[ENTER] keystroke will run the cell that you have selected and then select the next cell in the notebook without you having to use your mouse.
Run each cell one at a time using [SHIFT]-[ENTER].
Pause a moment to predict in your mind what is going to happen next.
Then run the cell and see if your predictions were roughly correct.
In your mind pay attention to the difference in what you expected and what actually happened.
This will gradually improve your ability to "run" the Python code in your head.
You can't *write* Python code until you are able to *read* Python code.
un all the Python cells in the notebook, paying attention to the discussion of the error messages and data cleaning steps.

### 3. Find the comments in Python cells and the instructions above them.

Comments begin with a hash symbol ( `#` ) and they should be highlighted in a different color than the rest of the Python code.

### 4. Fix the first error message (Traceback) that appears

If you are being careful with your active learning you might have been able to anticipate the error message.
Now you just need to fix it.
Your first code challenge is to fix the error messages that are created by the code in the Notebook. 
Error messages and Tracebacks are your friend. 
Computers do exactly what you *tell* them to do, never what you *want* them to do.
Error messages are the computers way of helping you see that what you told it to do was impossible or not in a language it understands.

You will need to "fill in the blanks" to make the code run correctly.
The comments in the Python cells can be a clue.
But the error messages are your best clues.
Python comments begin with the hash character `#`.
If you ever see this character in code all the text after it is assumed to be noncode - comments the developer is writing to explain things.
Now check out the scikit-learn documentation or use the built-in `help()` function to learn how to fit a `LinearRegression` model to your variables X and y which contain your feature variables and target variables (columns of data).

### 5. Calculate Root Mean Square Error

The second quiz question requires you to use the comments in the cells near the bottom to help you compute the RMSE (root mean square error) for your model's predictions.
This will help you answer the second quiz question.

### 6. Discussion Questions

1. Where you able to create an sklearn.LinearRegression model to predict weight from a person's height alone?
2. What was the standard error, or standard deviation of the error (RMSE) for your univariate linear regression that predicts weight from height?
3. What was the standard error for the second model you trained by fitting it to multiple features besides just height (sex, age, ...)? 
4. If you had more information in your database, such as zip code, ethnicity, occupation, that you used to train a model, would it be more or less accurate?
5. Is a univariate linear regression likely be more or less accurate than a polynomial regression (assume you use your training set as your test set)?
