# Parse number words in text

Would you like to help us build a Math Quiz Bot for K-12 students in Africa? You can grow your Python skills by helping us build a `text2int` function that we will use to process student chat messages. You can play with a [Hugging Face Spaces app](https://huggingface.co/spaces/TangibleAI/mathtext) or check out these Stack Overflow [answers](https://stackoverflow.com/q/493174/623735) to give you ideas.

It will be a [human vs machine](https://en.wikipedia.org/wiki/John_Henry_(folklore)) experiment to see if LLMs are helpful for you. We'll ask people that submit correct code how they got there so we can blog about it with you and promote your coding skill.

#### [mathtext.py](https://gitlab.com/tangibleai/community/team/-/edit/main/exercises/2-mathtext/mathtext/mathtext.py)
```python
def text2int(text):
    """ Convert an English str containing number words into an int

    >>> text2int("nine")
    9
    >>> text2int("forty two")
    42
    >>> text2int("1 2 three")
    123
    """
    pass
```

### Prizes

* **Deadline:** Monday, Jan 9 -- 10 PM GMT (2 PM US/Pacific)
* **Prizes:** $230

You have until Monday to complete the `mathtext.text2int(text)` Python function [here](https://gitlab.com/tangibleai/community/team/-/edit/main/exercises/2-mathtext/mathtext/mathtext.py).
The top submission will be rewarded $150 and we will pay you on a remote work app of your choice.
We will give you a glowing review to boost your career as a Python developer.
The top five valid submissions will be awarded $20 which can be paid however you like.

## Instructions

1. Fork this repository to your personal account
2. Copy the mathtext directory and rename it: `cp -r team/exercises/2-mathtext/mathtext/ team/exercises/2-mathtext/$YOUR_NAME/`
3. Add some test cases to the data files in your copy of `mathtext/test_text2int.*`
4. Complete the Python code snippet in your copy of `mathtext/text2int.py`
5. Push your code and data to your fork on GitLab
6. Create a merge request to this repository
7. E-Mail us a link to your Merge Request (engineering@tangibleai.com) with the subject "mathtext"

Your submissions will be scored and ranked based on two things:

### 1. Number of new test examples (50% of your score)

Using the example test cases in the docstring above, can you think of some larger numbers or edge cases?
What English words would a teenager in Africa use when chatting with a math quiz bot?
Add your examples to your copy of the example test set: `test_text2int.csv`.

Your submission will be scored based on the number of your test cases that the other submissions fail to answer correctly but do work in your code.

### 2. Accuracy (50% of your score)

1. Fork this repository
2. Clone your fork.
3. Copy the enclosed `mathtext/` directory to a new directory named for yourself (or your GitLab username): e.g. `cp -r team/exercises/2-mathtext/mathtext exercises/2-mathtext/hobs`.
4. You may install and use any of the Python packages listed in `2-mathtext/requirements.txt` 
5. Finish writing the function definition in `2-mathtext/$YOUR_NAME/mathtext.py` so that it works for the 3 examples in the docstring (9, 42, 123).
6. You may want to improve your function definition to work with some of the tests in the text files here (`mathtext/test_*.*`)
7. You may want to improve your function to be able to handle the new test examples you created earlier.

## Optional

For your own growth, feel free to submit an answer to the popular Stack Overflow question [convert-number-words-to-integers](https://stackoverflow.com/q/493174/623735)
