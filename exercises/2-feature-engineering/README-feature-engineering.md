# Feature Engineering

There are more than 11 kinds of data in data science and machine learning.
You will spend your career learning how to engineer numerical features out each of these data types.

1. continuous numerical
2. categorical
3. ordinal (ordered categorical)
4. tags (multi-label or nonexclusive categories)
5. date/time (time series)
6. location/gis
7. image
8. natural language (NLP)
9. audio or numerical sequences
10. video
11. spatio-temporal data (e.g. weather)

If you find a dataset that has something other than one of these kinds of data, add it to this list!

Practice your feature engineering skills by attempting to create a machine learning model that predicts home prices in Portland based on their characteristics or features.

## Dataset

To get started with feature engineering you need to get familiar with the dataset first.

1. Download the dataset (`.csv.gz`)
2. Open an ipython console (a python REPL)
3. In ipython load the data into a DataFrame 
4. If you like you can rename the columns to make them easier to work with
5. Compute some descriptive statistics (mean, standard deviation, number of unique values, ...) for each column in the DataFrame

Each column is called a "feature variable."
These are the features you are going to engineer.
Feature engineering is just converting values into numbers that represent the contents of that variable.
You need to convert all these columns to numbers so that a machine can "understand" them.
But before you do that you need to know what kind of data is in each.

1. Which of the 11 kinds of data science features listed above can you find in this dataset?
2. What is your feature engineering and data cleaning process for each of these kinds of features?

Start with the continuous numerical features and work your way up to the more challenging ones.

[portland_housing.csv.gz](./portland_housing.csv.gz)

## References

DON'T LOOK AT THIS! Try to do things your own way!

[Kaggle Dataset](https://www.kaggle.com/datasets/threnjen/portland-housing-prices-sales-jul-2020-jul-2021?resource=download)
