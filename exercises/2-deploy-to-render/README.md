# Render.com

Render is a modern, developer friendly version of Heroku.
With their render.yaml "blueprints" you can install virtually any debian packages you need.
And this can help to avoid lock-in.
Their free plan, like heroku's, suspends your VM whenever it is unused for 15 minutes or more.
But their paid plans are reasonable.
And they gave Tangible AI $10k of credit to support our nonprofit/social-impact work.

## SSH

SSH is not supported except for paid servers ($7/mo or more).

The browser-based SSH client works fine.
But sometimes you need to debug by examining files, variables, and applications directly on the server using SSH.

Unfortunately I couldn't connect to our VMs, even on a paid account... 
I had to change my ssh-keys from rsa to ecdsa or ed25519 ... they only mention this quirk at the bottom of their ssh troubleshooting guide. Render's ssh host doesn't support openssh `id_rsa` keys.
I verified that both the short ed25519 keys and the slightly longer ecdsa keys work fine.

Here are the proper steps from render docs: https://render.com/docs/ssh-generating-keys

And here's some more information if you want to try to reuse an existing key: https://render.com/docs/ssh-keys

But on Linux you'll need to avoid RSA keys.

```bash
# -N ""                       # new password is empty string
# -t ecdsa                    # the key type is ecdsa
# -f ~/.ssh/id_ecdsa.qary.ai  # path to private key (pub is *.pub)

ssh-keygen -N "" -t ecdsa -f ~/.ssh/id_ecdsa.qary.ai
```

The password and key type are important.
You should not enter any password (just hit enter at any password prompt).
And you should use `ecdsa` or `ed25519` type keys.
The file name doesn't matter, but make sure you'll understand what it means when you come back to it months from now.
Most key files start with `id_`.
Some key-pairs come in a single `.pem` file.
Others are separated into public keys (`*.pub`) and private keys (no extension).

## Add your key to your render account

Display your public key in the terminal so you can highlight all of the key text, copy it and paste it into your profile on render.com.
When you paste it into render, I like to give it a name that describes the computer and path where the key file is, such as `"hobs@laptop:~/.ssh/id_ecdsa.qary.ai"`.

```bash
cat ~/.ssh/ecdsa_render_staging.qary.ai
```

Then you need to edit your `~/.ssh/config` file if you don't want to have to remember the server/VM ID (which is used as the username in render's SSH connection).
You can find the server ID and the full ssh command on the settings page for your server on render, under the SHELL tab on the left drawer.

[![./render-ssh-command.png](./render-ssh-command.png)](./render-ssh-command.png)

Make sure you replace the `User` and `HostName` values based on what your dashboard tells you for your user account on render.
You will also need to use whatever file path you used for the `-f` option in the `keygen` command.

```
Host renderqary
    HostName ssh.oregon.render.com
    User srv-caalql76d9tp0fpbBk1q 
    IdentityFile ~/ed25519_render_qary.ai
```

That's it.
Now you can ssh into the Host using the shortcut you gave if for the "Host" value at the top of the `.ssh/config` section for this server.

```bash
ssh renderqary
```

## References

- [docs/ssh-troubleshooting](https://render.com/docs/ssh-troubleshooting)
- [docs/ssh](https://render.com/docs/ssh)
- [docs/ssh-generating-keys](https://render.com/docs/ssh-generating-keys)
- [docs/ssh-keys](https://render.com/docs/ssh-keys) (if you use a keyring on Mac)

nano ~/.ssh/config
nano ~/.ssh/config_qary
ls ~/.ssh//config_render_qary.ai 
nano config_render_qary.ai 
nano ~/.ssh/config_render_qary.ai 
ls -0 ~/.ssh/id_rsa_*
ls -1 ~/.ssh/id_rsa_*
ls -1 ~/.ssh/id_rsa_*qary*
git status
ls ~/.ssh/config_
ls ~/.ssh/config_*
ls -1 ~/.ssh/config_*
nano ~/.ssh/config
ls -1 ~/.ssh/config_*
nano ~/.ssh/config
ssh qary
ssh-add -l
more ~/.ssh/id_rsa_render_staging.qary.ai
more ~/.ssh/id_rsa_render_staging.qary.ai.pub
ssh-add -l
man ssh-add
ssh-add -L
cat ~/.ssh/id_rsa_render_staging.qary.ai.pub
ssh-add -L | grep AAAAB3NzaC1yc2EAAAADAQABAAABgQDE/bTWyXHVwh3rMgoKV3s9SO41
ssh-add -l
ssh -v qary
nano ~/.ssh/config_render_qary.ai 
ssh -v renderqary
nano ~/.ssh/config
ssh-keygen -l
ssh-keygen --help
ssh-keygen -t ed25519 -f ~/ed25519_render_qary.ai
nano ~/.ssh/config_render_qary.ai 
ls ~/.ssh/ed*
ls -hal
mv ~/ed25519_render_qary.ai ~/.ssh/
mv ~/ed25519_render_qary.ai.pub ~/.ssh/
cat ~/.ssh/ed25519_render_qary.ai.pub 
cat ~/.ssh/ed25519_render_qary.ai

ssh-keygen -t ecdsa -f ~/.ssh/ecdsa_render_qary.ai
cat ~/.ssh/ecdsa_render_qary.ai
cat ~/.ssh/ecdsa_render_qary.ai.pub
cat ~/.ssh/ecdsa_render_qary.ai.pub
nano ~/.ssh/config_render_qary.ai 
ssh renderecdsa

```
