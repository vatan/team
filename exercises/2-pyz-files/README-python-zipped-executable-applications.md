# Python Zipped Executable Applications

## Build a Python Zip Application

A Python Zip application is any ZIP file containing a __main__.py module (python source file).
The `__main__.py` dilfe works as the application's entry point.
When you run the application, Python automatically adds its container (the ZIP file itself) to sys.path.
This way `__main__.py` can import objects from the dependendencies - the modules and packages the application needs to run.

You can build a zipped python app by zipping it up and optionally adding a shabang line to the .zip file:

1. create your `app_name.py` python script
2. `mkdir app_name`
3. `cp app_name.py app_name/__main__.py`
4. `zip -r app_name.pyz app_name/`
5. for linux: `{ echo '!#/usr/bin/env python' ; cat app_name.pyz ; } > app_name.pyz`
6. chmod +x app_name.pyz
