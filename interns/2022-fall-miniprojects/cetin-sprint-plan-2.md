# Cetin Test Project Sprint 2

- [X] C1: Create a postgres DB on render.com
- [X] C2: Deploy BookAPP to render.com
- [X] C2: Create a simple chat app
- [X] C1: Create a redis DB on render.com
- [X] C1: Deploy chat app on render.com
- [ ] C1: Create a model for quiz app
- [X] C1: Create a consumer for quiz app
- [X] C1: Create a route for quiz app
- [X] C1: Build a logic for quiz app
- [X] C2: Integrate yaml file
- [X] C1: Integrate feedback for wrong answer
- [X] C1: Test quiz app locally
- [X] C1: Create a merge request


## Sprint 3

- [X] update yaml file
- [X] load yaml into database locally
- [X] load yaml into database on render for Cetin's local account
- [X] load yaml into database on render for qary.ai on render

# - [ ] update instead of overwrite data in database

### Enhancements

- [X] additional correct strings for 1+1: two, 2
- [X] additional accepted strings for start: begin, ready

- [X] if yaml file fails to load try making state names unique by using "quizbot-" prefix on all state names
