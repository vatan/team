# Sprint 1

## Project Description
**Project:** "Django-Delvin"

**Implementer:** Alex

**Project Description:**

Delvin is an analytics platform that ingests message data from chatbots to help nonprofit organizations analyze the activity happening in their chatbot.  We have incorporated several platforms into the project, including Landbot, TextIt, and Turn.io.  We would like to incorporate more platforms, including Telegram.


**Sprint Goal**

There are two issues with platform intergrations that need to be fixed in the Django-delvin application.

**Sprint Tasks:**

- Preparation Steps
  * [ ] Set up an account on Gitlab
  * [ ] Send Greg your Gitlab id.
  * [ ] Set up an account on [Render.com](https://render.com/)
  * [ ] Review the [Django Project Process document](https://gitlab.com/tangibleai/team/-/blob/main/exercises/resources/django-project-processes.md) to understand the project architectures and processes we are working towards
  * [ ] Give Alex access to the [Django-Delvin repository](https://gitlab.com/tangibleai/django-delvin) (after getting his Gitlab id).
  * [ ] Clone the project, and work on the [feature-platform-connections branch](https://gitlab.com/tangibleai/django-delvin/-/tree/feature-platform-connections).
  
- Issue 1

  * On line 130, there is a problem accessing the 'messages' content in the `data_json` object we get from Turn.io.  While the integration still seems to be processing requests correctly, we don't want to receive this error message.
    ```
    File "/opt/render/project/src/maitag/platforms/turn.py", line 130, in label_and_resubmit_message
    Nov 17 01:41:41 PM      message = data_json['messages'][0]['text']['body']
    Nov 17 01:41:41 PM  KeyError: 'messages'
    ```
  * [ ] Review the `intent-recognition-overview.md` in `django-delvin/docs`:  [link](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/intent-recognition-overview.md) to understand the context of this file and function.  Consider the "Request Object" section in particular.
  * [ ] Update the code on line 130 (or any other necessary code that is contributing to that error) to make sure the code runs without an error message.

- Issue 2
  * [ ] Make a [Landbot account ](https://landbot.io/)
  * [ ] The Landbot integration is throwing a [500 error](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#5xx_server_errors).  Diagnose the cause of this error and try to correct it.
      ```
      Nov 17 01:54:19 PM  10.204.127.219 - - [17/Nov/2022:04:54:19 +0000] "POST /api/turn/label/ HTTP/1.1" 500 96025 "-" "Turn/4.282.1"
      ```
  * [ ] Review the [Landbot Message Hook documentation](https://gitlab.com/tangibleai/django-delvin/-/blob/main/docs/landbot-webhook.md) to understand the context of this function.
  * [ ] Use this Landbot chatbot to test your fix.  [Sample Landbot](https://landbot.online/v3/H-1407248-4MTWHDI0R7TMEVVD/index.html)
    - This Landbot is hooked up to the site, so you don't need to follow the directions to make your own (unless you want to).
    - The Landbot is limited to 100 conversations per month.  If you leave the tab open and continue chatting with it as you work, it will only count as 1 conversation.  Closing the conversation and visiting the link again will count as a separate conversation.
