## Cetin's Mini Project Report

### Sprint 0

#### Task
- Create a simple BookAPP with Django

#### Actions
- I created a Django project
- I created a BookAPP
- Made migrations, and migrated
- Used the program in the local computer

#### Overview
The aim of this sprint was to create a simple Django project. To understand main functionalities of Django.

#### Learning Outcomes

With this sprint I've learned how to create a super user account, create tables, and to use admin pages of Django, and how start and stop a Django APP.

#### Book List
![Books](./pictures/book-books.png?raw=true "Title")

#### Book Read
![Read](./pictures/book-read.png?raw=true "Title")

### Sprint 1

#### Tasks
- Clone nudger
- Run nudger localy
- Create a FAQ APP

#### Actions
- I cloned the nudger repository
- Installed dependencies
- Created a local .env file
- Started the APP and tried features of it
- Created a new APP "FAQBot"
- The answer to a question is in json format, 
- There are two options for an answer, 
  - Response for "Who are you?": "I'm Qary!"
  - Response for anything else: "I don't know that!"

#### Overview
The tasks in this sprint were to run nudger on the local computer. And after that to add new functionality to it.

#### Learning Outcomes
In this sprint I've learned what nudger is used for. How to run nudger on the local machine. How to create a FAQBot using Django channels, and how to use Django models to store data on a database.

#### Question View
![Question](./pictures/faq-question.png?raw=true "Title")

#### Response View
![Response](./pictures/faq-response.png?raw=true "Title")

#### Question List View
![Questions](./pictures/faq-questions.png?raw=true "Title")

#### Nudger Repository
[nudger](https://gitlab.com/tangibleai/nudger)

#### FAQBot
[faqbot](https://www.qary.ai/faq/)

### Sprint 2

#### Tasks
- Deploy an APP on render.com
- Create a chat APP
- Create a route, consumer
- Build a logic

#### Actions
- I deployed BookAPP on render.com. And learned the deployment of a Django APP
- I created a chat APP with routes and consumers
- Created a simple template which uses websocket

#### Overview
The main task on this sprint was to deploy an APP on 'render.com'.

The secondary task was to create a basic chat APP which consumes websockets.

#### Learning Outcomes
With the main task I learned how to deploy an APP on 'render.com'. How to create environmental variables, and how to create a build file.

The secondary task helped me to understand the differences between HTTP and websocket connections.


#### Quizbot Start Page
![Quizbot Start](./pictures/quizbot-start.png?raw=true "Title")

#### Quizbot Chat
![Quizbot Chat](./pictures/quizbot-chat.png?raw=true "Title")

### Sprint 3

#### Tasks
- Create a "math.yaml" file
- Save "math.yaml" to local database
- Use chat_async consumers in your consumer
- Connect to math.js bundle

#### Actions
- I created a "math.yaml" file
- Saved "math.yaml" to local database
- Deployed simple version of quizbot on render.com
- Copied consumers, and models from chat_async to quizbot
- Connected quizbot and math.js bundle
- Tested quizbot locally

#### Overview
The aim of this sprint was to understand chat bots, the structure of yaml files, and the relations between a js bundle and django consumers.

#### Learning Outcomes

I learned how to create a yaml file. How a client is connecting with a server using websockets, and how Django can handle HTTP and websocket connections.

#### Quizbot Chat
![Quizbot Poly](./pictures/quizbot-poly.png?raw=true "Title")

#### Quizbot
[quizbot](https://www.qary.ai/quiz/)
