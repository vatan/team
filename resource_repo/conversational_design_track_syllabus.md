## Week 1

Learn about Tangible AI and what we do: https://us02web.zoom.us/rec/share/P0FQIaaoUoaTfpqWTAJbHl6qq6vqZwViXwibKQjL1NWC2DGWqR0L2S3myHWd_JOr.RyUdZgIc4lbdVuRZ?startTime=1580437251000

Hear a beginner lecture about conversational design (Maria's): https://drive.google.com/drive/folders/16mRieOLhUE4wMVPeC0ry-TabYXtZp1cz?usp=sharing

Hear a beginner lecture about conversational design (Effective Copywriting for Chatbots): https://www.youtube.com/watch?v=49G58PQWO7w 

Task: Play with at least 2-3 bots from https://tangibleai.com/social-impact-chatbot-database/. Answer the following questions for yourself: 
* Which chatbot did you like the best?
* What is one thing you would improve about each chatbot?


## Week 2-3 - Designing your chatbot  

Choose what you're going to design.
* Choose a Syndee skill to design (see instructions here: )
* Choose a bot to design from a list of ideas 

Independent chatbot: 

Syndee Skill : 

Use the conversational design format to design your skill: https://docs.google.com/spreadsheets/d/1MB-B06cj-g_RsvqLXC2TCmGJLFXLpTUzCKVBsnCUu-E/edit?usp=sharing

Additional Materials to read: 
Nielsen's Usability Heuristics (https://www.nngroup.com/articles/ten-usability-heuristics/) and their application for conversation design: https://www.evernote.com/l/AOz5BbIDigtC56zuzrsYhz7HlpVmErK4ffs/

## Week 4 - Building your skill


