# ProAI Pitch

## Goals

- 3 levels in playground.proai.org
- incorporate as nonprofit
- working donation button
- patreon?
- kickstarter?
- regular (monthly) blog posts
- qary version of `nudger` on render.com
- chat scripts for handling intern onboarding
- playground on render.com
- gitpod instance on render.com

## Backlog

- search engine that uses meillisearch (search.qary.ai)
- drawio instance on render.com
